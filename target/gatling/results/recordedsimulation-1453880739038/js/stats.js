var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "5",
        "ok": "2",
        "ko": "3"
    },
    "minResponseTime": {
        "total": "13",
        "ok": "13",
        "ko": "994"
    },
    "maxResponseTime": {
        "total": "1012",
        "ok": "58",
        "ko": "1012"
    },
    "meanResponseTime": {
        "total": "617",
        "ok": "35",
        "ko": "1004"
    },
    "standardDeviation": {
        "total": "475",
        "ok": "22",
        "ko": "7"
    },
    "percentiles1": {
        "total": "994",
        "ok": "35",
        "ko": "1008"
    },
    "percentiles2": {
        "total": "1008",
        "ok": "46",
        "ko": "1010"
    },
    "percentiles3": {
        "total": "1011",
        "ok": "55",
        "ko": "1011"
    },
    "percentiles4": {
        "total": "1011",
        "ok": "57",
        "ko": "1011"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 2,
        "percentage": 40
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 3,
        "percentage": 60
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.072",
        "ok": "0.029",
        "ko": "0.043"
    }
},
contents: {
"req_request-0-684d2": {
        type: "REQUEST",
        name: "request_0",
path: "request_0",
pathFormatted: "req_request-0-684d2",
stats: {
    "name": "request_0",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "1012",
        "ok": "-",
        "ko": "1012"
    },
    "maxResponseTime": {
        "total": "1012",
        "ok": "-",
        "ko": "1012"
    },
    "meanResponseTime": {
        "total": "1012",
        "ok": "-",
        "ko": "1012"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "1012",
        "ok": "-",
        "ko": "1012"
    },
    "percentiles2": {
        "total": "1012",
        "ok": "-",
        "ko": "1012"
    },
    "percentiles3": {
        "total": "1012",
        "ok": "-",
        "ko": "1012"
    },
    "percentiles4": {
        "total": "1012",
        "ok": "-",
        "ko": "1012"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 1,
        "percentage": 100
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.014",
        "ok": "-",
        "ko": "0.014"
    }
}
    },"req_request-1-46da4": {
        type: "REQUEST",
        name: "request_1",
path: "request_1",
pathFormatted: "req_request-1-46da4",
stats: {
    "name": "request_1",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "994",
        "ok": "-",
        "ko": "994"
    },
    "maxResponseTime": {
        "total": "994",
        "ok": "-",
        "ko": "994"
    },
    "meanResponseTime": {
        "total": "994",
        "ok": "-",
        "ko": "994"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "994",
        "ok": "-",
        "ko": "994"
    },
    "percentiles2": {
        "total": "994",
        "ok": "-",
        "ko": "994"
    },
    "percentiles3": {
        "total": "994",
        "ok": "-",
        "ko": "994"
    },
    "percentiles4": {
        "total": "994",
        "ok": "-",
        "ko": "994"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 1,
        "percentage": 100
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.014",
        "ok": "-",
        "ko": "0.014"
    }
}
    },"req_request-2-93baf": {
        type: "REQUEST",
        name: "request_2",
path: "request_2",
pathFormatted: "req_request-2-93baf",
stats: {
    "name": "request_2",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "1008",
        "ok": "-",
        "ko": "1008"
    },
    "maxResponseTime": {
        "total": "1008",
        "ok": "-",
        "ko": "1008"
    },
    "meanResponseTime": {
        "total": "1008",
        "ok": "-",
        "ko": "1008"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "1008",
        "ok": "-",
        "ko": "1008"
    },
    "percentiles2": {
        "total": "1008",
        "ok": "-",
        "ko": "1008"
    },
    "percentiles3": {
        "total": "1008",
        "ok": "-",
        "ko": "1008"
    },
    "percentiles4": {
        "total": "1008",
        "ok": "-",
        "ko": "1008"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 1,
        "percentage": 100
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.014",
        "ok": "-",
        "ko": "0.014"
    }
}
    },"req_request-3-d0973": {
        type: "REQUEST",
        name: "request_3",
path: "request_3",
pathFormatted: "req_request-3-d0973",
stats: {
    "name": "request_3",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "58",
        "ok": "58",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "58",
        "ok": "58",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "58",
        "ok": "58",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "58",
        "ok": "58",
        "ko": "-"
    },
    "percentiles2": {
        "total": "58",
        "ok": "58",
        "ko": "-"
    },
    "percentiles3": {
        "total": "58",
        "ok": "58",
        "ko": "-"
    },
    "percentiles4": {
        "total": "58",
        "ok": "58",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.014",
        "ok": "0.014",
        "ko": "-"
    }
}
    },"req_request-4-e7d1b": {
        type: "REQUEST",
        name: "request_4",
path: "request_4",
pathFormatted: "req_request-4-e7d1b",
stats: {
    "name": "request_4",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "13",
        "ok": "13",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "13",
        "ok": "13",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "13",
        "ok": "13",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "13",
        "ok": "13",
        "ko": "-"
    },
    "percentiles2": {
        "total": "13",
        "ok": "13",
        "ko": "-"
    },
    "percentiles3": {
        "total": "13",
        "ok": "13",
        "ko": "-"
    },
    "percentiles4": {
        "total": "13",
        "ok": "13",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.014",
        "ok": "0.014",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
